package yt;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fasterxml.jackson.databind.ObjectMapper;

import yt.dao.UserDAO;
import yt.dao.VideoDAO;
import yt.model.User;
import yt.model.Video;

public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		User loggedInUser = (User) session.getAttribute("loggedInUser");
		
		List<Video> videos = new ArrayList<>();
		String status = "";
		
		if(loggedInUser == null) {
			status = "guest";
		}
		videos = VideoDAO.getAll();

		List<User> topFive = UserDAO.getMostPopular();
		
		Map<String, Object> data = new HashMap<>();
		data.put("videos", videos);
		data.put("loggedInUser", loggedInUser);
		data.put("topFive", topFive);
		data.put("status", status);
		
		ObjectMapper mapper = new ObjectMapper();
		String jsonData = mapper.writeValueAsString(data);
		
		response.setContentType("application/json");
		response.getWriter().write(jsonData);
		
	}
}
