$(document).ready(function(){
	
	$.get('HomeServlet', function(data){
		if(window.location.search.substring(1).includes("search")){ 
			var array = [];
			var searchOption = window.location.search.substring(1).split("=")[1];
			for(var i=0;i<data.videos.length;i++) {
				if(data.videos[i].name.toLowerCase().includes(searchOption.toLowerCase())){
					array.push(data.videos[i]);
				}
			}
			data.videos=array;
		}
		if(window.location.search.substring(1).includes("sort")){ 
			data = sortVideos(data);
		}
		for(v in data.videos){
			if(data.loggedInUser == null || (data.loggedInUser.role != 'ADMIN')){
				if(data.videos[v].visibility == 'PUBLIC' && data.videos[v].deleted == false && data.videos[v].owner.blocked == false && data.videos[v].owner.deleted == false){
					$('.recommended').append('<div id="videoHome">'+
							'<div class="thumbnailWrapper">'+
								'<a href="video.html?id='+ data.videos[v].id +'"><img src="'+data.videos[v].videoImg+'" id="thumbnail"></a>'+
							'</div>'+
							'<a href="video.html?id='+ data.videos[v].id +'" id="naslov">' + data.videos[v].name + '</a>'+
							'<a href="profile.html?id='+ data.videos[v].owner.username +'" id="user">'+ data.videos[v].owner.username +'</a>'+
							'<span id="views">'+ data.videos[v].views +' views</span>'+
							'<span id="date">'+ data.videos[v].date +'</span>'+
						'</div>')
				}
			}else if(data.loggedInUser.role == 'ADMIN'){
				$('.recommended').append('<div id="videoHome">'+
						'<div class="thumbnailWrapper">'+
							'<a href="video.html?id='+ data.videos[v].id +'"><img src="'+data.videos[v].videoImg+'" id="thumbnail"></a>'+
						'</div>'+
						'<a href="video.html?id='+ data.videos[v].id +'" id="naslov">' + data.videos[v].name + '</a>'+
						'<a href="profile.html?id='+ data.videos[v].owner.username +'" id="user">'+ data.videos[v].owner.username +'</a>'+
						'<span id="views">'+ data.videos[v].views +' views</span>'+
						'<span id="date">'+ data.videos[v].date +'</span>'+
					'</div>')
			}
			
		}
		
		for(u in data.topFive){
			$('#top').append('<div id="osoba"><div id="korisnickoIme">'+
					'<a href="profile.html?id=' + data.topFive[u].username + '">' + data.topFive[u].username + '</a></div>'+
					'<div id="foloveri">'+ data.topFive[u].subsNumber +' followers</div>'+
					'</div>');
		}
	});
	
	$('#sort').on('click', function(){
		$("#myDropdown").toggle("show");
	});
	
	$('#searchbtn').on('click', function(event){
		var searchText = $('.srchinput').val();
		window.location.replace("home.html?search="+searchText);
		
	});
	
});

function sortVideos(data){
	
	var sortOption = window.location.search.substring(1).split("=")[1];
	
	if(sortOption=="TitleUp"){

		data.videos.sort(function(a, b) {
		    
			var x = a.name.toLowerCase();
		    var y = b.name.toLowerCase();
		    if (x < y) {return -1;}
		    if (x > y) {return 1;}
		    return 0;
			
		});
	}
	if(sortOption=="TitleDown"){
		
		data.videos.sort(function(a, b) {
		    
			var x = b.name.toLowerCase();
		    var y = a.name.toLowerCase();
		    if (x < y) {return -1;}
		    if (x > y) {return 1;}
		    return 0;
			
		});
	}
	if(sortOption=="AuthorUp"){
			
			data.videos.sort(function(a, b) {
			    
				var x = a.owner.username.toLowerCase();
			    var y = b.owner.username.toLowerCase();
			    if (x < y) {return -1;}
			    if (x > y) {return 1;}
			    return 0;
				
			});
		}
	if(sortOption=="AuthorDown"){
		
		data.videos.sort(function(a, b) {
		    
			var x = b.owner.username.toLowerCase();
		    var y = a.owner.username.toLowerCase();
		    if (x < y) {return -1;}
		    if (x > y) {return 1;}
		    return 0;
			
		});
	}
	if(sortOption=="ViewsDown"){
		data.videos.sort(function(a, b) {
			return a.views - b.views;
		});
	}
	if(sortOption=="ViewsUp"){
		data.videos.sort(function(a, b) {
			return b.views - a.views;
		});
	}
	if(sortOption=="DateCreatedUp"){
		data.videos.sort(function(a, b) {
			
			var brojGodinaA = parseInt(a.created.year);
			var brojMeseciA = odrediMesec(a.created.month);
			var brojDanaUtomMesecuA = parseInt(a.created.dayOfMonth);
			var ukupanBrojDanaA = (brojGodinaA * 365)+(brojMeseciA*30)+brojDanaUtomMesecuA;
			

			var brojGodinaB = parseInt(b.created.year);
			var brojMeseciB = odrediMesec(b.created.month);
			var brojDanaUtomMesecuB = parseInt(b.created.dayOfMonth);
			var ukupanBrojDanaB = (brojGodinaB * 365)+(brojMeseciB*30)+brojDanaUtomMesecuB;
			
			return ukupanBrojDanaA - ukupanBrojDanaB;
		});
	}
	if(sortOption=="DateCreatedDown"){
		data.videos.sort(function(a, b) {
			
			var brojGodinaA = parseInt(a.created.year);
			var brojMeseciA = odrediMesec(a.created.month);
			var brojDanaUtomMesecuA = parseInt(a.created.dayOfMonth);
			var ukupanBrojDanaA = (brojGodinaA * 365)+(brojMeseciA*30)+brojDanaUtomMesecuA;
			

			var brojGodinaB = parseInt(b.created.year);
			var brojMeseciB = odrediMesec(b.created.month);
			var brojDanaUtomMesecuB = parseInt(b.created.dayOfMonth);
			var ukupanBrojDanaB = (brojGodinaB * 365)+(brojMeseciB*30)+brojDanaUtomMesecuB;
			
			return ukupanBrojDanaB - ukupanBrojDanaA;
		});
	}
	
	return data;	
}