$(document).ready(function(){
	$.get('AdminServlet', function(data){
		console.log(data);
		
		var loggedInUser = data.loggedInUser;
		var users = data.users;
		
		if(loggedInUser == null || loggedInUser.role != 'ADMIN' || data.status == 'neMoze'){
			$('#users').empty();
            $('#users').html("<p>Ne mozete pristupiti ovoj stranici</p>");
		} else {
			for(u in users){
				$('#users').append('<tr>'+
					'<td><div id="user">'+
						'<a href="profile.html?id='+users[u].username +'">'+users[u].username +'</a>'+
						'<p><span>'+users[u].name +'</span> <span>'+users[u].lastname +'</span></p>'+
						'<p>'+users[u].email +'</p>'+
					'</div></td>'+
					'<td><div id="user">'+
						'<p>Reg. date: '+users[u].registrationDate +'</p>'+
						'<p>Deleted: '+users[u].deleted +'</p>'+
						'<p>Blocked: '+users[u].blocked +'</p>'+
					'</div></td>'+
					'<td><div id="user">'+
						'<p>Role: '+users[u].role +'</p>'+
						'<p>Subs number: '+users[u].subsNumber +'</p>'+
					'</div></td>'+
				'</tr>');			
		 }
			
			$('#delete').on('click', function(){
				var d = confirm('Da li ste sigurni da zelite da obrisete korisnika?');
				if(d == true){
					console.log('obrisan jee');
				}
			});
		}	
	});
});