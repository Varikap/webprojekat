$(document).ready(function(e){
	
	$.get('LoggedInServlet', function(data) {	
		var loggedInUser = data.loggedInUser;
		var status = data.status;
		
		if (status == "loggedIn"){
			if(loggedInUser.role == "ADMIN"){
				$('#sign').append('<a href="profile.html?id='+loggedInUser.username+'" id="profile">' + loggedInUser.username + '</a> <span>/</span> <a href="admin.html" id="profile">All users</a>'+
				' <span>/</span> <a href="register.html?doing=edit&id='+loggedInUser.username+'" id="profile">Edit profile</a> <span>/</span> <a href="LogoutServlet" id="signout">Sign out</a>');
			} else {
				$('#sign').append('<a href="profile.html?id='+loggedInUser.username+'" id="profile">' + loggedInUser.username + '</a> <span>/</span> '+
				'<a href="register.html?doing=edit&id='+loggedInUser.username+'" id="profile">Edit profile</a> <span>/</span> <a href="LogoutServlet" id="signout">Sign out</a>');
			}
			
		} else {
			$('#sign').append('<a href="register.html?doing=add&id=0" id="register">Register</a><span>/</span><a href="login.html" id="signin">Sign in</a>');
		}
	});

	function validateForm(){
        var a=document.forms["Form"]["username"].value;
        var b=document.forms["Form"]["password"].value;
        if (a==null || a=="" || b==null || b=="")
        {
            alert("Molim vas unesite potrebne podatke.");
            return false;
        }
    }
});