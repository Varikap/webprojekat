DROP SCHEMA IF EXISTS youwatchit;
CREATE SCHEMA youwatchit DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE youwatchit;

CREATE TABLE users (
	#id INT AUTO_INCREMENT,
	username VARCHAR(10) NOT NULL, 
	password VARCHAR(20) NOT NULL,
    name VARCHAR(20),
    lastname VARCHAR(20),
    email VARCHAR(30) NOT NULL,
    description VARCHAR(100),
    registrationDate DATETIME DEFAULT CURRENT_TIMESTAMP,
    role ENUM('USER', 'ADMIN') NOT NULL DEFAULT 'USER',
    blocked BIT NOT NULL DEFAULT 0,
    deleted BIT NOT NULL DEFAULT 0,
    subsNumber INT DEFAULT '0',
    likeVideo INT DEFAULT '0',
    dislikeVideo INT DEFAULT '0',
    likeComment INT DEFAULT '0',
    dislikeComment INT DEFAULT '0',
	 
    PRIMARY KEY(username)
);


CREATE TABLE videos (
	id INT AUTO_INCREMENT,
	name VARCHAR(100) NOT NULL,
	videoURL VARCHAR(100) NOT NULL,
	videoImg VARCHAR(300) DEFAULT 'https://www.sjrealtor.com/wp-content/themes/techism/assets/images/thumbnail.png',
	description VARCHAR(1000),
	visibility ENUM('PUBLIC', 'UNLISTED', 'PRIVATE') NOT NULL DEFAULT 'PUBLIC',
	commentsAllowed BOOLEAN DEFAULT true,
	ratingAllowed BOOLEAN DEFAULT true,
	blocked BOOLEAN DEFAULT false,
	deleted BOOLEAN DEFAULT FALSE,
	views INT NOT NULL DEFAULT 0,
	likes INT NOT NULL DEFAULT 0,
	dislikes INT NOT NULL DEFAULT 0,
	dateCreated DATETIME DEFAULT CURRENT_TIMESTAMP,
	user_id VARCHAR(10) NOT NULL,
    
	PRIMARY KEY (id),
	FOREIGN KEY (user_id) REFERENCES users(username));
    
    
CREATE TABLE comments (
	id INT AUTO_INCREMENT,
    content VARCHAR(100) NOT NULL,
    comment_date DATETIME DEFAULT CURRENT_TIMESTAMP,
    author VARCHAR(10) NOT NULL,
    videoId INT NOT NULL,
    likesNumber INT NOT NULL DEFAULT 0,
    dislikesNumber INT NOT NULL DEFAULT 0,
    deleted BOOLEAN DEFAULT false,
    
    PRIMARY KEY (id),
    FOREIGN KEY (author) REFERENCES users(username),
    FOREIGN KEY (videoId) REFERENCES videos(id)
);


CREATE TABLE videoRatings (
	id INT AUTO_INCREMENT,
    liked BOOLEAN DEFAULT true,
    rated_time DATETIME DEFAULT CURRENT_TIMESTAMP,
    who_rated VARCHAR(10) NOT NULL,
    rated_video INT NOT NULL,

	PRIMARY KEY (id),
    FOREIGN KEY (who_rated) REFERENCES users(username) ON DELETE CASCADE,
    FOREIGN KEY (rated_video) REFERENCES videos(id) ON DELETE CASCADE
);


CREATE TABLE commentRatings (
	id INT AUTO_INCREMENT,
    liked BOOLEAN DEFAULT true,
    rated_time DATETIME DEFAULT CURRENT_TIMESTAMP,
    who_rated VARCHAR(10) NOT NULL,
    rated_comment INT NOT NULL,
    
    PRIMARY KEY (id),
    FOREIGN KEY (who_rated) REFERENCES users(username) ON DELETE CASCADE,
    FOREIGN KEY (rated_comment) REFERENCES comments(id) ON DELETE CASCADE
);


INSERT INTO users (username, password, name, lastname, email, registrationDate, role) VALUES ('admin1', 'a', 'aa', 'aa', 'a@mail', '1.1.2000', 'ADMIN');
INSERT INTO users (username, password, name, lastname, email, registrationDate, role) VALUES ('admin2', 'a', 'bb', 'bb', 'a@mail', '1.1.2000', 'ADMIN');
INSERT INTO users (username, password, name, lastname, email, registrationDate, role) VALUES ('c', 'c', 'cc', 'cc', 'a@mail', '1.1.2000', 'USER');
INSERT INTO users (username, password, name, lastname, email, registrationDate, role) VALUES ('d', 'd', 'dd', 'dd', 'a@mail', '1.1.2000', 'USER');
INSERT INTO users (username, password, name, lastname, email, registrationDate, role) VALUES ('e', 'e', 'ee', 'ee', 'a@mail', '1.1.2000', 'USER');
INSERT INTO users (username, password, name, lastname, email, registrationDate, role) VALUES ('f', 'f', 'ff', 'ff', 'a@mail', '1.1.2000', 'USER');
INSERT INTO users (username, password, name, lastname, email, registrationDate, role) VALUES ('g', 'g', 'gg', 'gg', 'a@mail', '1.1.2000', 'USER');
INSERT INTO users (username, password, name, lastname, email, registrationDate, role, blocked) VALUES ('blok', 'b', 'blokiran', 'bbb', 'a@mail', '1.1.2000', 'USER', 1);
INSERT INTO users (username, password, name, lastname, email, registrationDate, role, deleted) VALUES ('del', 'd', 'obrisan', 'ddd', 'a@mail', '1.1.2000', 'USER', 1);


INSERT INTO videos (name, videoURL, videoImg, description, visibility, user_id, deleted) VALUES 
	('public video', 'https://www.youtube.com/embed/Ag1AKIl_2GM', 'https://i.ytimg.com/vi/CrknCxqAm0k/maxresdefault.jpg', 'rms at ted talk', 'PUBLIC', 'c', 0),
	('unlisted video', 'https://www.youtube.com/embed/iyaqSAotL3w', 'https://i.ytimg.com/vi/iyaqSAotL3w/maxresdefault.jpg', 'is amd good option now?', 'UNLISTED', 'c', 0),
	('private video', 'https://www.youtube.com/embed/sfrYOWlKJ_g', 'https://i.ytimg.com/vi/sfrYOWlKJ_g/maxresdefault.jpg', 'why louise do not use apple', 'PRIVATE', 'c', 0),
	('public', 'https://www.youtube.com/embed/K9u8zFVjX1g', 'https://i.ytimg.com/vi/K9u8zFVjX1g/maxresdefault.jpg', 'lofi', 'PUBLIC', 'c', 0),
	('public', 'https://www.youtube.com/embed/AEZbYcXSCXs', 'https://i.ytimg.com/vi/AEZbYcXSCXs/maxresdefault.jpg', 'ez', 'PUBLIC', 'e', 0),
    ('video blokiranog korisnika', 'https://www.youtube.com/embed/45bBYZmmjpY', 'https://i.ytimg.com/vi/PT1lG9hDBDA/maxresdefault.jpg', 'bunker rush', 'PUBLIC', 'blok', 0),
    ('video obrisanog korisnika', 'https://www.youtube.com/embed/6KGmLkFwq6I', 'https://i.ytimg.com/vi/6KGmLkFwq6I/maxresdefault.jpg', 'planetary fortress rush!!!', 'PUBLIC', 'del', 0),
	('obrisan video', 'https://www.youtube.com/embed/_yxhBT-nR9c', 'https://i.ytimg.com/vi/_yxhBT-nR9c/maxresdefault.jpg', 'NUKES!', 'PUBLIC', 'g', 1);


INSERT INTO comments (content, author, videoId) VALUES ('komentar 1', 'f', 2);
INSERT INTO comments (content, author, videoId) VALUES ('komentar 2', 'c', 1);
INSERT INTO comments (content, author, videoId) VALUES ('komentar 3', 'f', 2);
INSERT INTO comments (content, author, videoId) VALUES ('komentar 4', 'c', 1);
INSERT INTO comments (content, author, videoId) VALUES ('komentar 5', 'c', 2);
INSERT INTO comments (content, author, videoId) VALUES ('komentar 6', 'd', 1);
INSERT INTO comments (content, author, videoId) VALUES ('komentar 7', 'c', 2);
INSERT INTO comments (content, author, videoId) VALUES ('komentar 8', 'f', 2);


INSERT INTO videoRatings (liked, rated_time, who_rated, rated_video) VALUES (true, '2013-12-12', 'f', 1);
INSERT INTO videoRatings (liked, rated_time, who_rated, rated_video) VALUES (true, '2013-12-12', 'c', 2);
INSERT INTO videoRatings (liked, rated_time, who_rated, rated_video) VALUES (true, '2013-12-12', 'c', 1);
INSERT INTO videoRatings (liked, rated_time, who_rated, rated_video) VALUES (true, '2013-12-12', 'f', 3);
INSERT INTO videoRatings (liked, rated_time, who_rated, rated_video) VALUES (true, '2013-12-12', 'f', 1);
INSERT INTO videoRatings (liked, rated_time, who_rated, rated_video) VALUES (false, '2013-12-12', 'f', 1);
INSERT INTO videoRatings (liked, rated_time, who_rated, rated_video) VALUES (false, '2013-12-12', 'f', 1);


INSERT INTO commentRatings (liked, rated_time, who_rated, rated_comment) VALUES (true, '2013-12-12', 'f', 1);
INSERT INTO commentRatings (liked, rated_time, who_rated, rated_comment) VALUES (true, '2013-12-12', 'c', 2);
INSERT INTO commentRatings (liked, rated_time, who_rated, rated_comment) VALUES (true, '2013-12-12', 'c', 1);
INSERT INTO commentRatings (liked, rated_time, who_rated, rated_comment) VALUES (false, '2013-12-12', 'f', 3);
INSERT INTO commentRatings (liked, rated_time, who_rated, rated_comment) VALUES (true, '2013-12-12', 'f', 4);








